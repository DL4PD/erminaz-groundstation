#!/usr/bin/env python3

import binascii
import socket

UDP_IP = "127.0.0.1"
UDP_PORT = 16982
FILENAME = "./fixtures/qubik2/data_20210830T153147"

with open(FILENAME, 'rb') as f:
    data = f.read()

print("UDP target IP: {}".format(UDP_IP))
print("UDP target port: {}".format(UDP_PORT))
print("message: {}".format(str(data)))

sock = socket.socket(socket.AF_INET,    # Internet
                     socket.SOCK_DGRAM) # UDP
sock.sendto(data, (UDP_IP, UDP_PORT))
